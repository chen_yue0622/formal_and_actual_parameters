﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Ss1 ss1 = new Ss1();
            //行参/实参
            ss1.print();
            //ref类型
            int a = 20;
            bool re = ss1.ref1(ref a);
            Console.WriteLine($"验证结果是：{re}" );
            //out类型
            bool ot;
            ss1.out1(21, out ot);
            Console.WriteLine($"验证结果是：{ot}");
        }
    }
}
