﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Parameter parameter = new Parameter();
            var s = 22;
            Console.WriteLine(parameter.ParameterSomething(ref s));
            int r = 22;
            parameter.ParameterAnything(out r);
            Console.WriteLine(r);
        }
    }
}
