﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ref_out_params参数区别
{
    class Program
    {
        static void Main(string[] args)
        {
            //ref 参数使形参按引用传递 out 关键字用来定义输出函数
            int x= 3;
            int y = 5;
            int z;
            //Student student = new Student();
            //Console.WriteLine("计算结果 " + student.Add(ref x, y));
            //Console.WriteLine("实参x的值是 " + x);

            //Console.WriteLine("计算结果 " + student.Chen(x, y, out z ));
            //Console.WriteLine("实参z的值是 " + z);
            
            bool a = int.TryParse("1234", out x);
            if (a)
            {
                Console.WriteLine("转换成功");
            }
            else
            {
                Console.WriteLine("转换失败");
            }
            Console.ReadKey();

            Program program = new Program();
            int[] d = { 2, 13, 24, 46, };
            Console.WriteLine("运算结果 "+program.Add (d));
            Console.ReadKey();

        }
        private int Add(params int[] d)
        {
            int result = 0;
            for (int i = 0; i <d.Length; i++)
            {
                result += d[i];
            }
            return result;
            
        }

    }
   

}
