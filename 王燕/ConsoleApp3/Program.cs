﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] a = new int[3,3]{ { 56, 78, 89 }, { 68, 85, 86 }, { 98, 55, 65 } };
            ChinkClass cc = new ChinkClass();
            cc.BaseClass(ref a);
            cc.Add(ref a);
            _ = cc.Name(out _);
            Console.WriteLine(cc.Name(out _));
            _ = cc.Age(out _);
            Console.WriteLine("我的年龄："+Math.Abs(cc.Age(out _)));
           
        }
    }
}
