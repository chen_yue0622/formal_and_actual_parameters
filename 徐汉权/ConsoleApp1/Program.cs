﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
              public static int _number = 10;

        static void Main(string[] args)
        {
            //判断是否为闰年
            int a = 3;
            int res = Test(a);
            Console.WriteLine(res);
            bool b = IsRun(2007);
            //           （实参）

            //判断两个数字谁大谁小
            Console.WriteLine(b);
            int a1 = 10;
            int a2 = 20;
            GetMax(a1, a2);
            //    （实参）

            //判断用户输入的是数字
            Console.WriteLine("是否进入游戏？\n如要进入请输入：进入游戏\n如要退出请输入：退出游戏");
            string str = Console.ReadLine();
            string number = Usering(str);
            //                     (实参)
            Console.WriteLine(number);
            Console.ReadKey();
        }

        public static int Test(int a)
        {
            return a = a + _number;
        }

        //判断是否为闰年
        public static bool IsRun(int year)
        //                  (形参)
        {
            bool b = (year % 400 == 0) || (year % 4 == 0 && year % 100 != 0);
            return b;
        }

        //比较两个数的大小
        public static int GetMax(int n1, int n2)
        //                        (形参)
        {
            int Get = n1 > n2 ? n1 : n2;
            return Get;
        }

        //判断用户输入YES或NO
        public static string Usering(string user)
             //                      （形参）
        {
            while (true)
            {
                if(user == "进入游戏")
                {
                    Console.WriteLine("正在进入游戏...");
                    return user;
                }else if(user == "退出游戏")
                {
                    Console.WriteLine("正在退出游戏...");
                }
                else
                {
                    Console.WriteLine("请输入“进入游戏”或是“退出游戏”");
                    user = Console.ReadLine();
                }
            }
        }
    }
    }
